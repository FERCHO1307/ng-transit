import { Moment } from 'moment';
import {DaysEnum} from "./enumerations/days-enum.model";


export interface IRule {
  plate?: string;
  localTime?: Moment;
  daysEnum?: DaysEnum;
}

export class Rule implements IRule {
  constructor(public plate?: string, public localTime?: Moment, public daysEnum?: DaysEnum) {}
}
