export interface IResponse {
  result: boolean;
  message?: string;
}

export class Response implements IResponse {
  constructor(public result: boolean, public message?: string) {}
}
