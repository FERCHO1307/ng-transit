import {Component, OnInit} from '@angular/core';
import {HttpResponse} from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';

import {IRule, Rule} from "./shared/model/rule.model";
import {RuleService} from "./service/rule.service";
import * as moment from 'moment';
import {IResponse, Response} from "./shared/model/response.model";
import {AnimationOptions} from "ngx-lottie";
import {AnimationItem} from "lottie-web";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isSaving = false;
  nowTime: Date;
  result: Response;

  editForm = this.fb.group({
    plate: [null, [Validators.required]],
    localTime: [null, [Validators.required]],
    daysEnum: [null, [Validators.required]]
  });

  optionsTrue: AnimationOptions = {
    path: '/assets/circulate.json',
  };

  optionsFalse: AnimationOptions = {
    path: '/assets/police.json',
  };

  animationCreated(animationItem: AnimationItem): void {
    console.log(animationItem);
  }

  constructor(protected ruleService: RuleService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.nowTime = new Date();
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const rule = this.createFromForm();
    this.subscribeToSaveResponse(this.ruleService.query(rule));
  }

  private createFromForm(): IRule {
    const hour = this.editForm.get(['localTime'])!.value.split(":", 2);
    const time = moment();
    return {
      ...new Rule(),
      plate: this.editForm.get(['plate'])!.value,
      localTime: time.set({hour: parseInt(hour.indexOf(0), 10), minute: parseInt(hour.indexOf(1), 10)}),
      daysEnum: this.editForm.get(['daysEnum'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IResponse>>): void {
    result.subscribe(
      (data) => this.onSaveSuccess(data.body),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(result): void {
    this.result = result;
    this.isSaving = false;
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

}
