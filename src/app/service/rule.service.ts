import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {IRule} from '../shared/model/rule.model';
import {DATE_TIME_FORMAT} from '../shared/constants/input.constants';
import {environment} from "../../environments/environment";
import {IResponse} from "../shared/model/response.model";


type EntityResponseType = HttpResponse<IResponse>;

@Injectable({ providedIn: 'root' })
export class RuleService {
  public resourceUrl = environment.URL + 'api/transit';

  constructor(protected http: HttpClient) {}

  query(rule: IRule): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(rule);
    return this.http
      .post<IResponse>(this.resourceUrl + '/validate-plate', copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  protected convertDateFromClient(rule: IRule): IRule {
    const copy: IRule = Object.assign({}, rule, {
      localTime: rule.localTime && rule.localTime.isValid() ? rule.localTime.format(DATE_TIME_FORMAT) : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    return res;
  }

}
